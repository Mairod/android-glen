package com.example.saint.firsttry.questions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.saint.firsttry.Question;
import com.example.saint.firsttry.R;

import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by dorianlods on 14/02/2017.
 */

public class QuestionAdapter  extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {

    private ArrayList<Question> questions;

    public QuestionAdapter(ArrayList<Question> questions) {
        this.questions = questions;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final Context ctx = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(ctx);
        final View questionItemView = inflater.inflate(R.layout.question_item, parent, false);


        return new QuestionViewHolder(questionItemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        final Question question = questions.get(position);


        Context ctx = holder.itemView.getContext();


        holder.questionText.setText(question.text);

        switch (question.difficulty) {
            case "easy":
                holder.questionText.setTextColor(ctx.getResources().getColor(R.color.colorText));
                holder.questionText.setBackgroundColor(ctx.getResources().getColor(R.color.colorEasy));
                break;
            case "medium":
                holder.questionText.setTextColor(ctx.getResources().getColor(R.color.colorText));
                holder.questionText.setBackgroundColor(ctx.getResources().getColor(R.color.colorMedium));
                break;
            default:
                holder.questionText.setTextColor(ctx.getResources().getColor(R.color.colorText));
                holder.questionText.setBackgroundColor(ctx.getResources().getColor(R.color.colorHard));
                break;
        }


        final MaterialDialog mMaterialDialog = new MaterialDialog(ctx)
                .setTitle("Answer")
                .setMessage("Go");

        mMaterialDialog.setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMaterialDialog.setMessage(question.answer);
                mMaterialDialog.show();

            }
        });






    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    class QuestionViewHolder  extends RecyclerView.ViewHolder {

        TextView questionText;

        public QuestionViewHolder(View itemView){

            super(itemView);
            this.questionText = (TextView) itemView.findViewById(R.id.question_text);

        }

    }

}
