package com.example.saint.firsttry.questions;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.saint.firsttry.Question;
import com.example.saint.firsttry.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.Request.Method.GET;

public class Questions extends AppCompatActivity {

    protected String convertCharacter(String string) {
        String result;
        result = string.replace("&quot;", "\"");
        return result.replace("&#039;", "'");
    }


    RecyclerView recyclerView ;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_questions);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.question_list);
        recyclerView.setLayoutManager(layoutManager);

        Integer numberOfQuestions = getIntent().getExtras().getInt("numberOfQuestions");

        this.fetchQuestions(numberOfQuestions);


//        recyclerView.setAdapter();

    }

    void fetchQuestions(int numberOfquestions){

        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "https://opentdb.com/api.php?amount=" + numberOfquestions;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try{

                    JSONArray results = response.getJSONArray("results");

                    final ArrayList<Question> questions = new ArrayList<>();


                    for (int i = 0; i < results.length(); i++){

                        String result = convertCharacter(results.getJSONObject(i).getString("question"));
                        String difficulty = results.getJSONObject(i).getString("difficulty");
                        String answer = results.getJSONObject(i).getString("correct_answer");
                        Question question = new Question(result, difficulty, answer);

                        questions.add(question);

                    }

                    recyclerView.setAdapter(new QuestionAdapter(questions));

                } catch (JSONException e){

                    e.printStackTrace();

                    Log.d("Echec", "onResponse: ");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(jsonRequest);

    }

}
