package com.example.saint.firsttry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.saint.firsttry.questions.Questions;

public class MainActivity extends AppCompatActivity {

    TextView myTextView;
    SeekBar actionSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        myTextView = (TextView) this.findViewById(R.id.text_view);
        actionSeekBar = (SeekBar) this.findViewById(R.id.seek_bar);
        Button questionButton = (Button) findViewById(R.id.question_button);

        actionSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                String data = String.valueOf(progress);
                myTextView.setText(data);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        questionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent secondeActivite = new Intent(MainActivity.this, Questions.class);
                secondeActivite.putExtra("numberOfQuestions", actionSeekBar.getProgress());
                startActivity(secondeActivite);

            }
        });
    }
}
