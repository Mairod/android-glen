package com.example.saint.firsttry;

import android.util.Log;

/**
 * Created by dorianlods on 07/03/2017.
 */

public class Question {
    public String text;
    public String difficulty;
    public String answer;

    public Question(String text, String difficulty, String answer ) {
        this.text = text;
        this.difficulty = difficulty;
        this.answer = answer;
    }

}
